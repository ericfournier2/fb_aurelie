#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################
#FONCTION POUR STATISTICS
calc_snp_stats <- function(geno)
{
     m <- nrow(geno)     ## number of snps
     n <- ncol(geno)     ## number of individuals
     geno[(geno!=0) & (geno!=1) & (geno!=2)] <- NA
     geno <- as.matrix(geno)
     n0 <- apply(geno==0,1,sum,na.rm=T)
     n1 <- apply(geno==1,1,sum,na.rm=T)
     n2 <- apply(geno==2,1,sum,na.rm=T)
     n <- n0 + n1 + n2
     p <- ((2*n0)+n1)/(2*n)
     q <- 1 - p
     maf <- pmin(p, q)
     mac = pmin(n0,n2)
     mgf <- apply(cbind(n0,n1,n2),1,min) / n
   res <- data.frame( n=n, n0=n0, n1=n1, n2=n2, maf=maf, mac=mac, stringsAsFactors=F )
     row.names(res) <- row.names(geno)
     res
}
#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################
calc_LD <- function( geno, inds=1:nrow(geno), get.D=F, get.Dprime=F, get.rsq=T, get.chisq=F, get.chisq_prime=F ) {
	if( all(!get.D, !get.Dprime, !get.rsq, !get.chisq, !get.chisq_prime) ) { stop('Must request at least one LD statistic.\n') }
	D_prime <- rsq <- chisq <- chisq_prime <- df <- NULL
	D <- matrix(NA, nrow=nrow(geno), ncol=length(inds))
	if( get.Dprime ) { D_prime <- matrix(NA, nrow=nrow(geno), ncol=length(inds)) }
	if( get.rsq ) { rsq <- matrix(NA, nrow=nrow(geno), ncol=length(inds)) }
	if( get.chisq | get.chisq_prime ) { 
		chisq <- matrix(NA, nrow=nrow(geno), ncol=length(inds))
		df <- matrix(NA, nrow=nrow(geno), ncol=length(inds)) 
		if( get.chisq_prime ) { chisq_prime <- matrix(NA, nrow=nrow(geno), ncol=length(inds)) }
	}
	
	if( all(as.logical(!is.na(geno))) ) {	#no missing data
		tmp.geno <- geno	## genotypes at locus A
		N <- ncol(tmp.geno)	#number of individuals (diploid is assumed)
		pA <- ((2*apply(tmp.geno==0,1,sum,na.rm=T))+apply(tmp.geno==1,1,sum,na.rm=T)) / (2*N)
		pa <- 1-pA
		for(i in 1:length(inds)) {
			tmp.Bgeno <- matrix(tmp.geno[inds[i],],nrow=nrow(tmp.geno),ncol=ncol(tmp.geno),byrow=T)	## genotypes at locus B
			pB <- ((2*apply(tmp.Bgeno==0,1,sum,na.rm=T))+apply(tmp.Bgeno==1,1,sum,na.rm=T)) / (2*N)
			pb <- 1-pB
			pAB <- ((apply(tmp.geno==0 & tmp.Bgeno==0, 1, sum,na.rm=T)*2) + (apply(tmp.geno==1 & tmp.Bgeno==0, 1, sum,na.rm=T)) + (apply(tmp.geno==0 & tmp.Bgeno==1, 1, sum,na.rm=T)) + (apply(tmp.geno==1 & tmp.Bgeno==1, 1, sum,na.rm=T)*0.5)) / (2*N) 
			D[,i] <- pAB-(pA*pB)
			if( get.Dprime ) { 
				Dmax <- pmin(pA*pb, pa*pB)
				Dmin <- pmax(-pA*pB, -pa*pb)
				pos <- (D[,i]>=0)
				D_prime[which(pos),i] <- D[which(pos),i] / Dmax[which(pos)]
				D_prime[which(!pos),i] <- D[which(!pos),i] / Dmin[which(!pos)]
			}
			if( get.rsq ) {
				rsq[,i] <- (D[,i]*D[,i]) / (pA*pa*pB*pb)
			}
			if( get.chisq | get.chisq_prime ) {
				chisq[,i] <- (2*N*D[,i]*D[,i]) / (pA*pa*pB*pb)
				if( get.chisq_prime ) {
					k=2-as.integer(pA==0|pa==0)
					m=2-as.integer(pB==0|pb==0)
					#df[,i] <- (k-1)*(m-1)
					chisq_prime[,i] <- chisq[,i] / (2*N*pmin(k,m))
				}
			}
		}	
	} else {	#at least one missing data point in geno
		for(i in 1:length(inds)) {
			tmp.geno <- geno[,!is.na(geno[inds[i],])]	## genotypes at locus A; i.e. all loci, but excluding samples with missing data at lcous B (i)
			tmp.Bgeno <- matrix(tmp.geno[inds[i],],nrow=nrow(tmp.geno),ncol=ncol(tmp.geno),byrow=T)	## genotypes at locus B (i.e. i-th locus); pulling from tmp.geno, so samples with missing data at i-th locus (B) will also be excluded
			tmp.Bgeno[is.na(tmp.geno)] <- NA 	#anytime where locus A (i.e. all non i-th locus) is missing, set as missing 
			N <- rowSums(!is.na(tmp.geno))
			pA <- ((2*apply(tmp.geno==0,1,sum,na.rm=T))+apply(tmp.geno==1,1,sum,na.rm=T)) / (2*N) 
			pB <- ((2*apply(tmp.Bgeno==0,1,sum,na.rm=T))+apply(tmp.Bgeno==1,1,sum,na.rm=T)) / (2*N)
			pa <- 1-pA
			pb <- 1-pB
			pAB <- ((apply(tmp.geno==0 & tmp.Bgeno==0, 1, sum,na.rm=T)*2) + (apply(tmp.geno==1 & tmp.Bgeno==0, 1, sum,na.rm=T)) + (apply(tmp.geno==0 & tmp.Bgeno==1, 1, sum,na.rm=T)) + (apply(tmp.geno==1 & tmp.Bgeno==1, 1, sum,na.rm=T)*0.5)) / (2*N) 
			D[,i] <- pAB-(pA*pB)
			if( get.Dprime ) { 
				Dmax <- pmin(pA*pb, pa*pB)
				Dmin <- pmax(-pA*pB, -pa*pb)
				pos <- (D[,i]>=0)
				D_prime[which(pos),i] <- D[which(pos),i] / Dmax[which(pos)]
				D_prime[which(!pos),i] <- D[which(!pos),i] / Dmin[which(!pos)]
			}
			if( get.rsq ) {
				rsq[,i] <- (D[,i]*D[,i]) / (pA*pa*pB*pb)
			}
			if( get.chisq | get.chisq_prime ) {
				chisq[,i] <- (2*N*D[,i]*D[,i]) / (pA*pa*pB*pb)
				k=2-as.integer(pA==0|pa==0)
				m=2-as.integer(pB==0|pb==0)
				df[,i] <- (k-1)*(m-1)
				if( get.chisq_prime ) {
					chisq_prime[,i] <- chisq[,i] / (2*N*pmin(k,m))
				}
			}
		}
	}
	if( !get.D ) { D <- NULL }
	if( !get.chisq ) { chisq <- NULL }
	return(list(D=D, Dprime=D_prime, rsq=rsq, chisq=chisq, chisq_prime=chisq_prime, chisq_df=df))
}
#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################